package internal_test

import (
	"livecampus-demo-app/internal"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDivision(t *testing.T) {
	result, err := internal.Division(56, 7)
	assert.Equal(t, nil, err)
	assert.NotEqual(t, nil, result)
	assert.Equal(t, float32(8), *result)
}

func TestDivisionDivisionBy0(t *testing.T) {
	result, err := internal.Division(12, 0)

	var expectedResult *float32

	assert.NotEqual(t, nil, err)
	assert.Equal(t, expectedResult, result)
}
