package internal

import "fmt"

func Division(first, second int) (*float32, error) {
	if second == 0 {
		return nil, fmt.Errorf("division by 0 exception")
	}

	ret := float32(first) / float32(second)
	return &ret, nil
}
